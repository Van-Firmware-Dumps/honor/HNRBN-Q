#! /vendor/bin/sh

#=============================================================================
# Copyright (c) 2022, 2023 Honor Device Co., Ltd.
# All Rights Reserved.
# Confidential and Proprietary - Honor Device Co., Ltd.
#=============================================================================

ddr_info=`sed -n 2p /proc/ddr_info`
if [[ "$ddr_info" == 0x4* ]]; then
    setprop ro.vendor.product.ddrsize 4
elif [[ "$ddr_info" == 0x6* ]]; then
    setprop ro.vendor.product.ddrsize 6
elif [[ "$ddr_info" == 0x8* ]]; then
    setprop ro.vendor.product.ddrsize 8
else
    setprop ro.vendor.product.ddrsize others
fi
